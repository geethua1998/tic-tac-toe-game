import random
print('Welcome to Tic Tac Toe!')
def dBoard(board):
    print('   |   |')
    print(' ' + board[7] + ' | ' + board[8] + ' | ' + board[9])
    print('   |   |')
    print('-----------')
    print(' ' + board[4] + ' | ' + board[5] + ' | ' + board[6])
    print('   |   |')
    print('-----------')
    print(' ' + board[1] + ' | ' + board[2] + ' | ' + board[3])
    print('   |   |')

def inputplayer():
    a= ''
    while not (a== 'X' or a== 'O'):
        print('Do you want to be X or O?')
        a= input().upper()
    if a== 'X':
         return ['X', 'O']
    else:
         return ['O', 'X']
def fturn():
    if random.randint(0, 1) == 0:
        return 'computer'
    else:
        return 'player'

def makeMove(board, letter, move):
    board[move] = letter
def winner(bo, le):
    return ((bo[7] == le and bo[8] == le and bo[9] == le) or (bo[4] == le and bo[5] == le and bo[6] == le) or (bo[1] == le and bo[2] == le and bo[3] == le) or (bo[7] == le and bo[4] == le and bo[1] == le) or (bo[8] == le and bo[5] == le and bo[2] == le) or (bo[9] == le and bo[6] == le and bo[3] == le) or (bo[7] == le and bo[5] == le and bo[3] == le) or  (bo[9] == le and bo[5] == le and bo[1] == le))

def freespace(board, move):
    return board[move] == ' '
def playermove(board):
    move = ' '
    while move not in '1 2 3 4 5 6 7 8 9'.split() or not freespace(board, int(move)):
        print('What is your next move? (1-9)')
        move = input()
    return int(move)

def computermove(board, computerLetter):
    if computerLetter == 'X':
        playerLetter = 'O'
    else:
        playerLetter = 'X'
    for i in range(1, 10):
        copy = bcopy(board)
        if freespace(copy, i):
            makeMove(copy, computerLetter, i)
            if winner(copy, computerLetter):
                return i
    for i in range(1, 10):
        copy = bcopy(board)
        if freespace(copy, i):
            makeMove(copy, playerLetter, i)
            if winner(copy, playerLetter):
                return i
    move = rmove(board, [1, 3, 7, 9])
    if move != None:
        return move
    if freespace(board, 5):
        return 5
    return rmove(board, [2, 4, 6, 8])
def rmove(board, movesList):
    p= []
    for i in movesList:
        if freespace(board, i):
            p.append(i)
    if len(p) != 0:
        return random.choice(p)
    else:
        return None
def checkfull(board):
    for i in range(1, 10):
        if freespace(board, i):
            return False
        else:
            return True
def bcopy(board):
    cboard = []
    for i in board:
        cboard.append(i)
    return cboard
def playagain():
    print('Do you want to play again? (yes or no)')
    return input().lower().startswith('y')

while True:
    theBoard = [' '] * 10
    #i=0
    #dict={}
    #l=[]
    playerLetter, computerLetter = inputplayer()
    turn = fturn()
    print('The ' + turn + ' will go first.')
    game = True
    while game:
            if turn == 'player':
                dBoard(theBoard)
                move = playermove(theBoard)
                makeMove(theBoard, playerLetter, move)
                if winner(theBoard, playerLetter):
                    dBoard(theBoard)
                    print('Hooray! You have won the game!')
                    game = False
                else:
                    if checkfull(theBoard):
                        dBoard(theBoard)
                        print('The game is a tie!')
                        break
                    else:
                        turn = 'computer'
            else:
                move = computermove(theBoard, computerLetter)
                makeMove(theBoard, computerLetter, move)
                if winner(theBoard, computerLetter):
                    dBoard(theBoard)
                    print('The computer has beaten you! You lose.')
                    game = False
                else:
                    if checkfull(theBoard):
                        dBoard(theBoard)
                        print('The game is a tie!')
                        break
                    else:
                        turn = 'player'
            #i=+1
            #l=[]
    if not playagain():
        break